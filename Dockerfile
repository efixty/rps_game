FROM amazoncorretto:11
RUN mkdir -p /opt/app
COPY target/rps.jar /opt/app
ENTRYPOINT java -Dspring.profiles.active=dev -XX:+UnlockExperimentalVMOptions -XX:+UseZGC -Xlog:gc -jar /opt/app/rps.jar