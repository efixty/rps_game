package pro.efiproj.rps.domain.response;

import lombok.Getter;
import pro.efiproj.rps.domain.GameOutcome;

@Getter
public class RpsGameResponse extends GenericTextResponse {

  private static final String winResponseTemplate = "%s\n%s win!\n%s";
  private static final String drawResponseTemplate = "%s\nLucky you, it was DRAW";

  private final GameOutcome outcome;
  private final String botChoice;

  private RpsGameResponse(Integer chatId, GameOutcome gameOutcome, String botChoice) {
    super(chatId);
    this.outcome = gameOutcome;
    this.botChoice = botChoice;
  }

  public static RpsGameResponseBuilder builder() {
    return new RpsGameResponseBuilder();
  }

  @Override
  public String toText() {
    return this.outcome != GameOutcome.DRAW ?
        String.format(
            winResponseTemplate,
            this.botChoice,
            this.outcome == GameOutcome.BOT_WIN ? "Bot" : "You",
            this.outcome == GameOutcome.BOT_WIN ? "Better luck next time" : "Congrats!"
        ) : String.format(
        drawResponseTemplate,
        this.botChoice
    );
  }

  public static class RpsGameResponseBuilder {

    private Integer chatId;
    private GameOutcome gameOutcome;
    private String botChoice;

    public RpsGameResponseBuilder withChatId(Integer chatId) {
      this.chatId = chatId;
      return this;
    }

    public RpsGameResponseBuilder withGameOutcome(GameOutcome gameOutcome) {
      this.gameOutcome = gameOutcome;
      return this;
    }

    public RpsGameResponseBuilder withBotChoice(String botChoice) {
      this.botChoice = botChoice;
      return this;
    }

    public RpsGameResponse build() {
      return new RpsGameResponse(this.chatId, this.gameOutcome, this.botChoice);
    }
  }
}
