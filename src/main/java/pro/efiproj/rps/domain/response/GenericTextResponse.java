package pro.efiproj.rps.domain.response;

import lombok.Getter;

@Getter
abstract public class GenericTextResponse {

  private final Integer chatId;

  protected GenericTextResponse(Integer chatId) {
    this.chatId = chatId;
  }

  public abstract String toText();
}
