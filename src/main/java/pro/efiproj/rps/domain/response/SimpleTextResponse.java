package pro.efiproj.rps.domain.response;

import lombok.Getter;
import pro.efiproj.rps.domain.payload.ScheduledClass;

import java.util.List;
import java.util.stream.Collectors;

@Getter
public class SimpleTextResponse extends GenericTextResponse {

    private static final String existingClassTextTemplate = "Prof: %s\n"
        + "Link: %s\n"
        + "Starts at %s\n"
        + "Ends at %s";
    private static final String noClassString = "de mi sksi";

    private final List<ScheduledClass> currentClasses;

    protected SimpleTextResponse(Integer chatId, List<ScheduledClass> scheduledClasses) {
        super(chatId);
        this.currentClasses = scheduledClasses;
    }

    public static SimpleTextResponseBuilder builder() {
        return new SimpleTextResponseBuilder();
    }

    @Override
    public String toText() {
        return currentClasses.isEmpty() ? noClassString : currentClasses
            .stream()
            .map(this::toSingleMessageText)
            .collect(Collectors.joining("\n-------\n"));
    }

    private String toSingleMessageText(ScheduledClass currentClass) {
        return String.format(
            existingClassTextTemplate,
            currentClass.getProfessorName(),
            currentClass.getLink(),
            currentClass.getStartTime().toString(),
            currentClass.getEndTime().toString()
        );
    }

    public static class SimpleTextResponseBuilder {

        private Integer chatId;
        private List<ScheduledClass> scheduledClasses;

        public SimpleTextResponseBuilder withChatId(Integer chatId) {
            this.chatId = chatId;
            return this;
        }

        public SimpleTextResponseBuilder withScheduledClasses(List<ScheduledClass> scheduledClasses) {
            this.scheduledClasses = scheduledClasses;
            return this;
        }

        public SimpleTextResponse build() {
            return new SimpleTextResponse(this.chatId, this.scheduledClasses);
        }
    }
}
