package pro.efiproj.rps.domain.botapi;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Message {
    private Integer message_id;
    private User from;
    private Integer date;
    private Chat chat;
    private String text;
}
