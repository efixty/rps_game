package pro.efiproj.rps.domain.botapi;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Chat {
    private Integer id;
    private String type;
    private String first_name;
    private String last_name;
    private String username;
}
