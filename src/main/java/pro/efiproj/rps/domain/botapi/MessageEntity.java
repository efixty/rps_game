package pro.efiproj.rps.domain.botapi;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MessageEntity {

    private String type;
    private Integer offset;
    private Integer length;
    private String url;
    private User user;
    private String language;

}
