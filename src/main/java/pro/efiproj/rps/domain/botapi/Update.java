package pro.efiproj.rps.domain.botapi;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Update {
    private Integer update_id;
    private Message message;
    private MessageEntity[] messageEntities;
}
