package pro.efiproj.rps.domain.payload;

import lombok.Builder;
import lombok.Getter;

import java.time.LocalTime;

@Getter
@Builder
public class ScheduledClass {
  private final String professorName;
  private final String link;
  private final LocalTime startTime;
  private final LocalTime endTime;

}
