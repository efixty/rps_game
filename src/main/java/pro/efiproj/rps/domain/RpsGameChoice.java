package pro.efiproj.rps.domain;

import lombok.Getter;

@Getter
public enum RpsGameChoice {
    ROCK("Rock"),
    PAPER("Paper"),
    SCISSORS("Scissors");

    private String name;

    RpsGameChoice(String name) {
        this.name = name;
    }
}
