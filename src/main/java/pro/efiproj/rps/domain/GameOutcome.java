package pro.efiproj.rps.domain;

import static pro.efiproj.rps.domain.RpsGameChoice.*;

public enum GameOutcome {
    BOT_WIN,
    DRAW,
    USER_WIN;

    public static GameOutcome determineGameOutcome(RpsGameChoice userChoice, RpsGameChoice botChoice) {
        if ((userChoice == ROCK && botChoice == PAPER)
                || (userChoice == PAPER && botChoice == SCISSORS)
                || (userChoice == SCISSORS && botChoice == ROCK)) return BOT_WIN;
        else if (userChoice == botChoice) return DRAW;
        else return USER_WIN;
    }
}
