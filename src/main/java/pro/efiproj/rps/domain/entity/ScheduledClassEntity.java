package pro.efiproj.rps.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pro.efiproj.rps.domain.payload.ScheduledClass;

import java.time.LocalTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "class")
public class ScheduledClassEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "professor_name")
  private String professorName;

  @Column(name = "link")
  private String link;

  @Column(name = "start_time")
  private LocalTime start;

  @Column(name = "end_time")
  private LocalTime end;

  @Column(name = "day_of_week")
  private int dayOfWeek;

  public ScheduledClass toPayload() {
    return ScheduledClass.builder()
        .link(this.link)
        .professorName(this.professorName)
        .startTime(this.start)
        .endTime(this.end)
        .build();
  }
}


