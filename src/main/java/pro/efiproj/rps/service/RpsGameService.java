package pro.efiproj.rps.service;

import org.springframework.stereotype.Service;
import pro.efiproj.rps.domain.GameOutcome;
import pro.efiproj.rps.domain.RpsGameChoice;
import pro.efiproj.rps.domain.botapi.Update;
import pro.efiproj.rps.domain.entity.ScheduledClassEntity;
import pro.efiproj.rps.domain.payload.ScheduledClass;
import pro.efiproj.rps.domain.response.GenericTextResponse;
import pro.efiproj.rps.domain.response.RpsGameResponse;
import pro.efiproj.rps.domain.response.SimpleTextResponse;
import pro.efiproj.rps.repo.NanoRepository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class RpsGameService {

    private static final Random rand = new Random();

    private final NanoRepository nanoRepository;

    public RpsGameService(NanoRepository nanoRepository) {
        this.nanoRepository = nanoRepository;
    }

    public GenericTextResponse processUpdate(Update update) {
        final int chatId = update.getMessage().getChat().getId();
        if (update.getMessage().getText().equals("/now")) {
            return SimpleTextResponse.builder()
                .withChatId(chatId)
                .withScheduledClasses(getRunningClass())
                .build();
        } else if (isRPSGame(update.getMessage().getText())) {
            final RpsGameChoice userChoice = RpsGameChoice
                .valueOf(update.getMessage().getText().toUpperCase().substring(1));
            final RpsGameChoice botChoice = RpsGameChoice.values()[rand.nextInt(3)];
            return RpsGameResponse.builder()
                .withChatId(chatId)
                .withBotChoice(botChoice.getName())
                .withGameOutcome(GameOutcome.determineGameOutcome(userChoice, botChoice))
                .build();
        } else {
            return RpsGameResponse.builder()
                .withChatId(chatId)
                .withBotChoice("inch es xosum e? shem jogum")
                .withGameOutcome(GameOutcome.DRAW)
                .build();
        }
    }

    private boolean isRPSGame(String text) {
        return text.equals("/paper") || text.equals("/scissors") || text.equals("/rock");
    }

    private List<ScheduledClass> getRunningClass() {
        return nanoRepository.findAllByStartBeforeAndEndAfterAndDayOfWeekEquals(
            LocalTime.now().plusMinutes(3),
            LocalTime.now(),
            LocalDate.now().getDayOfWeek().ordinal()
        )
            .stream()
            .map(ScheduledClassEntity::toPayload)
            .collect(Collectors.toList());
    }
}
