package pro.efiproj.rps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RpsBotStarter {
    public static void main(String[] args) {
        SpringApplication.run(RpsBotStarter.class);
    }
}
