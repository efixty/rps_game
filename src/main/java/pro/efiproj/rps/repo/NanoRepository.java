package pro.efiproj.rps.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pro.efiproj.rps.domain.entity.ScheduledClassEntity;

import java.time.LocalTime;
import java.util.List;

@Repository
public interface NanoRepository extends CrudRepository<ScheduledClassEntity, Long> {

    List<ScheduledClassEntity> findAllByStartBeforeAndEndAfterAndDayOfWeekEquals(
        LocalTime currentStart,
        LocalTime currentEnd,
        int dayOfWeek
    );
}
