package pro.efiproj.rps.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pro.efiproj.rps.domain.botapi.Update;
import pro.efiproj.rps.domain.response.GenericTextResponse;
import pro.efiproj.rps.service.RpsGameService;

import java.util.Date;

@Slf4j
@RestController
public class RpsGameController {

    private final RpsGameService service;
    private final MessageSender sender;

    public RpsGameController(RpsGameService service, MessageSender sender) {
        this.service = service;
        this.sender = sender;
    }

    @PostMapping("/update")
    public ResponseEntity<?> update(@RequestBody Update update) {
        if (update.getMessage() == null || update.getMessage().getText() == null) {
            return ResponseEntity.ok().build();
        }
        log.info("Got an update at " + new Date(update.getMessage().getDate()));
        GenericTextResponse generatedResponse = service.processUpdate(update);
        log.info("Generated response for update");
        sender.sendTextMessageParallel(generatedResponse);
        log.info("The response is scheduled to be sent");
        return ResponseEntity.ok().build();
    }
}
