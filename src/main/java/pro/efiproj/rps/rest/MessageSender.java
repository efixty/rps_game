package pro.efiproj.rps.rest;

import lombok.SneakyThrows;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Component;
import pro.efiproj.rps.domain.response.GenericTextResponse;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class MessageSender {

    private static final String URL = "https://api.telegram.org/bot1068944787:AAFr8iCPNokiIMYSf2_qG8w-PnJzPucG0Bs/sendMessage";

    private final ExecutorService executorService = Executors.newFixedThreadPool(
        Runtime.getRuntime().availableProcessors()
    );

    public void sendTextMessageParallel(GenericTextResponse textResponse) {
        executorService.submit(() -> sendTextMessage(textResponse));
    }

    @SneakyThrows
    public void sendTextMessage(GenericTextResponse textResponse) {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost postRequest = new HttpPost(URL);
        HttpEntity entity = EntityBuilder
            .create()
            .setParameters(parametersFromTextMessage(textResponse))
            .build();
        postRequest.setEntity(entity);
        CloseableHttpResponse response = client.execute(postRequest);
        System.out.println(response);
        response.close();
        client.close();
    }

    private List<NameValuePair> parametersFromTextMessage(GenericTextResponse genericResponse) {
        return Arrays.asList(
            new BasicNameValuePair("chat_id", genericResponse.getChatId().toString()),
            new BasicNameValuePair("text", genericResponse.toText())
        );
    }
}
